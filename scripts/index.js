'use strict'

let pets = [{
    name: "Rubby",
    type: "Dog",
    breed: "Corgi",
    bestTrick: "Tug of war",
    image: "images/rubby.jpg"
}, {
    name: "Howdy",
    type: "Dog",
    breed: "Mixed Breed",
    bestTrick: "Go find it!",
    image: "images/howdy.jpg"
}, {
    name: "KitKit",
    type: "Cat",
    breed: "American Shorthair",
    bestTrick: "Commanding his owner to feed him",
    image: "images/kitkit.jpg"
}, {
    name: "Lil' Miss",
    type: "Cat",
    breed: "Tabby",
    bestTrick: "Looking aloof",
    image: "images/lilmiss.jpg"
}, {
    name: "Happy",
    type: "Dog",
    breed: "Golden Retriever",
    bestTrick: "Refusing to retrieve!",
    image: "images/happy.jpg"
}, {
    name: "Piper",
    type: "Dog",
    breed: "Beagle",
    bestTrick: "Find it!  Dropped food edition!",
    image: "images/piper.jpg"
}, {
    name: "Spooky",
    type: "Cat",
    breed: "Mixed",
    bestTrick: "Gymnastics!",
    image: "images/spooky.jpg"
}];

let petsSorted = pets.sort(function (a, b) {
    if (a.name < b.name) {
        return -1;
    }
    if (a.name > b.name) {
        return 1;
    }
    return 0;
});

window.onload = function () {
    loadPetSelect();
}

function loadPetSelect() {
    const petList = document.getElementById("petList");
  
    let selectOption = new Option("Select a pet...", "");
    selectOption.onclick = clearCards;
    petList.appendChild(selectOption);

    let allOption = new Option("All Pets", "all pets");
    allOption.onclick = loadAllPets;
    petList.appendChild(allOption);

    let allDogsOption = new Option("All Dogs", "all dogs");
    allDogsOption.onclick = loadAllDogs;
    petList.appendChild(allDogsOption);

    let allCatsOption = new Option("All Cats", "all cats");
    allCatsOption.onclick = loadAllCats;
    petList.appendChild(allCatsOption);
  
    for (let i = 0; i < petsSorted.length; i++) {
      let petOption = new Option(petsSorted[i].name, petsSorted[i].name);
      petOption.onclick = changePet;
      petList.appendChild(petOption);
    }
  }
  
  function changePet() {
  
    const petList = document.getElementById("petList");
    const selectedIndex = petList.selectedIndex;
  
    for (let i = 0; i < petsSorted.length; i++) {
      if (petList[selectedIndex].value === petsSorted[i].name) {
        clearCards();
        addCard(petsSorted[i]);
      }
    }
  }

  function clearCards() {
    const petDiv = document.getElementById("petDiv");
    petDiv.innerHTML = "";
  }


function loadAllPets() {
    clearCards();
    for (let i = 0; i < petsSorted.length; i++) {
        //construct a card here
        addCard(petsSorted[i]);
    }
}

function loadAllDogs() {
    clearCards();
    for (let i = 0; i < petsSorted.length; i++) {
        //construct a card here
        if (petsSorted[i].type === "Dog") {
            addCard(petsSorted[i]);
        }        
    }
}

function loadAllCats() {
    clearCards();
    for (let i = 0; i < petsSorted.length; i++) {
        //construct a card here
        if (petsSorted[i].type === "Cat") {
            addCard(petsSorted[i]);
        } 
    }
}

function addCard(petStats) {

    const petDiv = document.getElementById("petDiv");

    //append Pet Card

    let card = document.createElement("div");
    card.className = "card mb-4";
    petDiv.appendChild(card);

    //append Pet Header for card    

    let cardHeader = document.createElement("div");
    cardHeader.className = "card-header py-3";
    card.appendChild(cardHeader);

    let cardHeading = document.createElement("h4");
    cardHeading.className = "mb-0";
    cardHeading.innerHTML = petStats.name;
    cardHeader.appendChild(cardHeading);

    //append card body for card

    let cardBody = document.createElement("div");
    cardBody.className = "card-body table-responsive text-center";
    card.appendChild(cardBody);

    //append img to card body
    let cardImg = document.createElement("img");
    cardImg.src = petStats.image;
    cardImg.alt = petStats.name;
    cardImg.className = "img-fluid";
    cardBody.appendChild(cardImg);

    //append stats table to body
    let petsTable = document.createElement("table");
    petsTable.className = "table table-striped border-bottom border-dark align-items-center";
    cardBody.appendChild(petsTable);

    // append table head to table
    let tableHead = document.createElement("thead");
    tableHead.className = "border-bottom border-dark border-3";
    petsTable.appendChild(tableHead);

    //append table head rows to table
    let tableHeadType = document.createElement("th");
    let tableHeadBreed = document.createElement("th");
    let tableHeadTrick = document.createElement("th");

    tableHeadType.innerHTML = "Pet Type";
    tableHeadBreed.innerHTML = "Breed";
    tableHeadTrick.innerHTML = "Best Trick";

    tableHead.appendChild(tableHeadType);
    tableHead.appendChild(tableHeadBreed);
    tableHead.appendChild(tableHeadTrick);

    // append table body to table
    let tableBody = document.createElement("tbody");
    petsTable.appendChild(tableBody);

    //add data row
    let row = tableBody.insertRow(-1);
    let cell1 = row.insertCell(0);
    let cell2 = row.insertCell(1);
    let cell3 = row.insertCell(2);

    cell1.innerHTML = petStats.type;
    cell2.innerHTML = petStats.breed;
    cell3.innerHTML = petStats.bestTrick;

}
